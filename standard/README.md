# Arch standard preinstallers

This folder includes packages that you would use every day, including a desktop environment and manager. For applications, it comes installed with a web browser, office suite, text editor, media players, and other utilities. It also includes a terminal emulator in there so you don't have to worry about getting a terminal installed if you forget to install one. Note that some of these packages are from Linux Mint, so you will get the same experience with these applications within Arch Linux.

Desktop environment: Cinnamon - A Linux desktop featuring a traditional layout, built from modern technology and introducing brand new innovative features.
> Open Source
https://github.com/linuxmint/cinnamon

Web browser: Vivaldi - An advanced browser made with the power user in mind.
Extra: vivaldi-ffmpeg-codecs - additional support for proprietary codecs for vivaldi
> Not fully open source, only UI is proprietary
https://vivaldi.com

Text Editor: Xed - A small and lightweight texst editor. X-Apps Project
> Open Source
https://github.com/linuxmint/xed

Image Viewer: xviewer [AUR] - A small and easy to use image viewer. X-Apps Project
> Open Source
https://linuxmint.com/xviewer

Video/MPV Player: Celluloid - Simple GTK+ frontend for mpv
> Open Source
https://github.com/linuxmint/celluloid

Video Downloader: youtube-dl - A command-line program to download videos from YouTube.com and a few more sites
> Open Source
https://youtube-dl.org

Music Player: Rhythmbox - Music playback and management application
> Open Source
http://www.rhythmbox.org
https://wiki.gnome.org/Apps/Rhythmbox

Terminal Emulator: gnome-terminal - The GNOME Terminal Emulator
> Open Source
https://wiki.gnome.org/Apps/Terminal

System information viewer: neofetch - A CLI system information tool written in BASH that supports displaying images.
> Open Source
https://github.com/dylanaraps/neofetch

Screenshot Utility: gnome-screenshot [source] - Take pictures of your screen
> Open Source, patched version from Linux Mint
https://github.com/linuxmint/gnome-screenshot
