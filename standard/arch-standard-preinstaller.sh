sudo clear
echo Welcome to the Arch Linux Standard Preinstaller! This will install the packages needed for a desktop environment and basic applications for everyday use. This will install the Cinnamon desktop environment on your desktop. If this is what you do not intend, STOP. Exit the installation and execute ./arch-standard-applications.sh instead. 
echo DO NOT EXECUTE THIS AS ROOT. When executing as root, there may be issues installing packages from AUR or building from source code. Make sure you execute this without the sudo command.
echo 
echo Before the installation, please make sure you are running the root account to ensure applications install properly. The system will perform updates to make sure you are up to date before the installation.
echo
echo This installer requires Aura to be installed in order for the installation to work. Do not attempt to install on any other linux distro that is not Arch Linux. If Aura is already installed, it will be updated if needed. Updates will be performed at this time.
echo Press Enter/Return to continue. Press [Ctrl+C] to exit.
head -n 1 >/dev/null
clear
echo Performing system updates...
echo
sudo pacman -Syu
echo Press Enter/Return to continue. Press [Ctrl+C] to exit.
head -n 1 >/dev/null
clear
echo What is Aura?
echo Aura is a package manager for Arch Linux. Its original purpose is as an AUR helper, in that it automates the process of installing packages from the Arch User Repositories. It is, however, capable of much more.
echo
echo Aura is Pacman
echo Aura does not just mimic pacman; it is pacman. All pacman operations and their sub-options are allowed. Some even hold special meaning in Aura as well.
echo
echo Arch is Arch - AUR is AUR
echo -S yields pacman packages and only pacman packages. This agrees with the above. In Aura, the -A operation is introduced for obtaining AUR packages. -A comes with sub-options you are used to [-u, -s, -i, etc.].
echo
echo Secure Package Building
echo PKGBUILDs from the AUR can contain anything. It is a users resposibility to verify the contents of a PKGBUILD before building, but people can make mistakes and overlook details. Aura scans PKGBUILDs before building to detect bash misuse and other exploits. The -P command is also provided for scanning your own PKGBUILDs.
echo Also, while pre-build PKGBUILD editing is not default behaviour, this can be achieved with --hotedit.
echo 
echo Downgradibility
echo Aura allows you to downgrade individual packages to previous versions with -C. It also handles snapshots of your entire system, so that you can roll back whole sets of packages when problems arise. The option -B will save a package state, and -Br will restore a state you select. -Su and -Au also invoke a save automatically.

echo More information and the source code about Aura can be found at
echo https://github.com/fosskers/aura
echo
echo Press Enter/Return to continue. Press [Ctrl+C] to exit.
head -n >/dev/null
echo Installing Aura package manager...
git clone https://aur.archlinux.org/aura-bin.git
cd aura-bin
makepkg
sudo pacman -U --noconfirm *.tar.zst
echo Aura Package Manager installed/updated.
echo Press Enter/Return to continue. Press [Ctrl+C] to exit.
head -n >/dev/null
sudo clear
echo The installer will install Cinnamon, LightDM, and xorg.
echo
echo Press Enter/Return to continue. Press [Ctrl+C] to exit.
head -n 1 >/dev/null
sudo aura -S --noconfirm xorg 
sudo aura -S --noconfirm lightdm lightdm-gtk-greeter
sudo systemctl enable lightdm.service
sudo aura -S --noconfirm cinnamon
sudo aura -S --noconfirm xdg-user-dirs
xdg-user-dirs-update
echo Desktop environment and manager installed.
echo Press Enter/Return to continue. Press [Ctrl+C] to exit.
head -n 1 >/dev/null
clear
echo These packages will be installed:
echo 
echo vivaldi - An advanced browser made with the power user in mind.
echo vivaldi-ffmpeg-codecs - additional support for proprietary codecs for vivaldi
echo xed - A small and lightweight text editor. X-Apps Project.
echo xviewer [AUR] - A small and easy to use image viewer. X-Apps Project.
echo libreoffice-fresh - LibreOffice branch which contains new features and program enhancements
echo celluloid - Simple GTK+ frontend for mpv
echo youtube-dl - A command-line program to download videos from YouTube.com and a few more sites
echo rhythmbox - Music playback and management application
echo gnome-terminal - The GNOME Terminal Emulator
echo neofetch - A CLI system information tool written in BASH that supports displaying images.
echo gnome-screenshot [source] [patched-linuxmint-version] - Take pictures of your screen
echo Press Enter/Return to continue. Press [Ctrl+C] to exit.
head -n 1 >/dev/null
sudo aura -S --noconfirm vivaldi vivaldi-ffmpeg-codecs
sudo aura -S --noconfirm xed
sudo aura -A --noconfirm xviewer
sudo aura -S --noconfirm libreoffice-fresh
sudo aura -S --noconfirm celluloid
sudo aura -S --noconfirm youtube-dl
sudo aura -S --noconfirm rhythmbox
sudo aura -S --noconfirm gnome-terminal
sudo aura -S --noconfirm neofetch
git clone https://github.com/linuxmint/gnome-screenshot
cd gnome-screenshot
sudo aura -S meson
cd build
ninja install
clear
echo The packages are installed. You will need to install a display driver for xorg to display the desktop properly.
echo If you are running on Intel integrated graphics, enter
echo sudo aura -S xf86-video-intel mesa
echo If you are running on AMD Graphics, enter
echo sudo aura -S xf86-video-amdgpu mesa
echo This installer does not provide a way to install proprietary nvidia drivers as of now. It may be implemented in the future, but you will have to ask around for help to installing proprietary drivers.
echo 
echo Thank you for using the arch-standard-preinstall script. We hope you have a good time with the packages we have installed for you. The last step is installing a display driver for xorg, and you should be able to reboot.
echo
echo Check out the git repo:
echo https://gitlab.com/tetrashot7/arch-scripts
echo
exit 0
