sudo clear
echo
echo Welcome to the Arch Linux desktop applications preinstaller! This will install the KDE Plasma desktop environment/SDDM along with some applications. 
echo
echo Please note that Aura Package Manager will be installed as it is required to run this script properly. Do not run this script as root as installing from AUR and running some scripts if present will not install if you are on root account.
echo
echo The script will apply updates before starting the script to make sure your system is up to date. If the system has upgraded any critical packages [aka linux kernels], make sure to reboot your system after running the script.
echo Press Enter to update and install Aura.
head -n 1 >/dev/null
echo Performing system update...
sudo pacman -Syu
echo Press Enter to continue.
head -n 1 >/dev/null
echo Installing Aura Package Manager...
git clone https://aur.archlinux.org/aura-bin.git
cd aura-bin 
makepkg
sudo pacman -U --noconfirm *.tar.zst
echo Aura Package Manager installed/updated.

sudo aura -S --needed plasma sddm
echo Checking for active and installed display managers...
sudo systemctl disable gdm && sudo aura -Rsu gnome
sudo systemctl disable lightdm && sudo aura -Rsu lightdm
sudo systemctl disable lxdm && sudo aura -Rsu lxdm
echo Current display managers disabled, removed, or not changed. Enabling SDDM...
sudo systemctl enable sddm
echo KDE Plasma/SDDM installed.
echo Installing applications...
sudo aura -S vivaldi vivaldi-ffmpeg-codecs flatpak libreoffice-fresh kate dolphin thunar xfce4-terminal alacritty vlc kvantum-qt5 elisa
echo Applications have been installed. The script will now exit. 
echo If you would like to start SDDM and log into KDE Plasma, press Enter now. If not, press Ctrl+C to exit. If you need to reboot, do so now.
head -n 1 >/dev/null
sudo systemctl start sddm
exit 0
