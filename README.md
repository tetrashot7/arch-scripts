# Welcome to the repository of Arch preinstallers!
Here, you can find a list of pre-configured installers that you can use to automatically install packages for your computer after you have installed Arch Linux or want to get applications installed.

Before you start, these Arch preinstallers require Aura to run properly. For more information on how to install Aura, follow this link:
https://github.com/fosskers/aura

UPDATE: I have added an installer that installs the standard applications. Based on the other script, but does not install the desktop environment or manager.

If you want a custom installer, let me know at anytime so I can build a script especially for you to install. Please note that Aura Package Manager will still be required for the scripts.

If you need any questions, let me know from my Discord.

TetraShot
`Discord: TetraShot#4015`
